python-rstr (3.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 30 Apr 2024 18:34:53 +0100

python-rstr (3.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Supports Python 3.11 (Closes: #1026569)
  * Build with pybuild-plugin-pyproject.
    - Patch to add a pyproject.toml - upstream hasn't shipped one, yet.
    - Point unittest at the tests in the source tree, as they aren't
      installed.
  * Bump Standards-Version to 4.6.2, no changes needed.
  * Declare Rules-Requires-Root: no.
  * Add autopkgtest.

 -- Stefano Rivera <stefanor@debian.org>  Tue, 10 Jan 2023 19:51:27 -0400

python-rstr (2.2.6-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Emmanuel Arias ]
  * d/control: Bump debhelper-compat to 13.
  * d/control: Bump Standards-Version to 4.6.0.
  * Update homepage URL moved (Closes: #992562).
    - d/watch: Update URL to the new repository.
    - d/copyright: Update Source url.
    - d/control: Update homepage.
  * d/salsa-ci.yml: enable salsa-ci.
  * d/copyright: fix reference to deprecated BSD license file.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Contact, Repository,
    Repository-Browse.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Emmanuel Arias <eamanu@yaerobi.com>  Mon, 20 Sep 2021 22:28:14 -0300

python-rstr (2.2.6-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 20:22:09 +0200

python-rstr (2.2.6-1) unstable; urgency=medium

  * Initial release (Closes: #877017)

 -- Ximin Luo <infinity0@debian.org>  Wed, 27 Sep 2017 21:49:22 +0200
