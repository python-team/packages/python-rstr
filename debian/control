Source: python-rstr
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ximin Luo <infinity0@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python3-all,
 python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/leapfrogonline/rstr
Vcs-Git: https://salsa.debian.org/python-team/packages/python-rstr.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-rstr
Rules-Requires-Root: no

Package: python3-rstr
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Easily generate random strings of various types (Python 3)
 rstr is a helper module for easily generating random strings of various
 types. It could be useful for fuzz testing, generating dummy data, or
 other applications. It has no dependencies outside the standard library,
 and is compatible with Python 3.
 .
 The basic method of rstr is rstr(). At a minimum, it requires one
 argument, an alphabet of characters from which to create a string.
 .
 Inspired by the Java library of the same name, the xeger() method allows
 users to create a random string from a regular expression.
 .
 You can combine rstr with Python's built-in string formatting to produce
 strings that fit a variety of templates.
 .
 This package installs the library for Python 3.
